# SAST Analyzer JavaScript sample
| 起動         | SAST Analyzer                                                                                | ジョブ名               | スキャナー                                                                                                                                                                                                                                                                                                                                      |
|------------|----------------------------------------------------------------------------------------------|--------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| deprecated | [ESLint analyzer](https://gitlab.com/gitlab-org/security-products/analyzers/eslint)          | `eslint-sast`      | <ul><li>[eslint-plugin-security](https://github.com/nodesecurity/eslint-plugin-security) ([検査ルール](https://github.com/nodesecurity/eslint-plugin-security#rules))<li>[eslint-plugin-react](https://github.com/jsx-eslint/eslint-plugin-react) ([検査ルール](https://github.com/jsx-eslint/eslint-plugin-react/tree/master/docs/rules) [^1])</ul> |
| 有効         | [NodeJsScan analyzer](https://gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan) | `nodejs-scan-sast` | [njsscan](https://github.com/ajinabraham/njsscan) ([検査ルール](https://github.com/ajinabraham/njsscan/tree/master/njsscan))                                                                                                                                                                                                                    |
| 有効         | [Semgrep analyzer](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep)        | `semgrep-sast`     | [Semgrep](https://semgrep.dev/) (検査ルール: [eslint-plugin-security](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/blob/main/rules/eslint.yml) からと [eslint-plugin-react](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/blob/main/rules/react.yml) から)                                              |

[^1]: 脆弱性検査のルールのみ。

SAST で検査した結果のレポートは[パイプライン](https://gitlab.com/sast-investigation/js-sast-sample/-/pipelines)からダウンロードしてください。

## サンプルファイル
* [vulnerability/eval.js](./vulnerability/eval.js) - 脆弱性のあるコード。
* [vulnerability/middleware.js](./vulnerability/middleware.js) - NodeJsScan analyzer テスト用。
* [vulnerability/xss.ejs](./vulnerability/xss.ejs) - NodeJsScan の EJS 検査テスト用。
* [pages/react.xss.js](./pages/react.xss.js) - eslint-plugin-react の検査テスト用。


## その他
### ESLint analyzer
* 検査エンジンに [ESLint](https://eslint.org/) を、検査ルールに [eslint-plugin-security](https://github.com/nodesecurity/eslint-plugin-security) と  を利用しています。
  - [eslint-plugin-security](https://github.com/nodesecurity/eslint-plugin-security)
  - [eslint-plugin-react](https://github.com/jsx-eslint/eslint-plugin-react) (の中の[脆弱性検査のルールのみ](https://gitlab.com/gitlab-org/security-products/analyzers/eslint/-/blob/master/eslintrc#L32-36))
* TypeScript にも対応しています。
* Semgrep analyzer への移植が完了したと判断され、deprecated になりました。今後バージョンアップが期待できなので、使わないほうが無難です。


### NodeJsScan analyzer
* NodeJsScan のコマンドライン版の njsscan を利用しています。
  - EJS などのテンプレートは正規表現で検査。
  - JavaScript のコードは Semgrep で検査。
* TypeScript には非対応。
* JavaScript は Express like な middleware のみ検査するようです。カスタムクラスなどの脆弱性は検出できません。


### Semgrep analyzer
* ESLint analyzer の検査ルールをもとに、GitLab 社が Semgrep ルールに移植したものです。
* TypeScript にも対応しています。
* Semgrep の仕様により、Security Code Scan から[移植されていない検出ルール](https://gitlab.com/gitlab-org/gitlab/-/issues/322192)があります。